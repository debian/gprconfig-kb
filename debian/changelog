gprconfig-kb (25.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 27 Nov 2024 18:36:22 +0000

gprconfig-kb (24.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Only autopkgtest static-lib-cross on amd64, gnat-*-alpha-linux-gnu is
    not available on all architectures. See #1056633.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 26 Nov 2023 20:21:44 +0000

gprconfig-kb (23.0.0-4) unstable; urgency=medium

  * Remove add-targets-for-gnat-s_osconf.diff.
    Gnat now stores System.OS_Constants.Target_Name without the vendor id.
  * Rewrite archive-builder.diff as upstream requested.
  * Improve gprconfig.patch:
    - detect mingw32 compilers. Closes: #1036069.
    - detect TARGET-gnatmake-VERSION without the unversioned TARGET-gnatmake
      (soon removed from native compilers).
  * Add autopkgtests for cross builds.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 23 Oct 2023 16:59:14 +0000

gprconfig-kb (23.0.0-3) unstable; urgency=medium

  * 23.0.0 introduces a gcc_version_major variable.  Adapt the patch so
    that, like gcc_version, it does not require the unversioned gcc link.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 28 Dec 2022 20:10:29 +0000

gprconfig-kb (23.0.0-2) unstable; urgency=medium

  * Reupload to unstable for the gnat-12 transition.
  * Standards-Version: 4.6.2.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 26 Dec 2022 23:26:49 +0100

gprconfig-kb (23.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 23 Nov 2022 21:15:54 +0000

gprconfig-kb (23~20220715-1) experimental; urgency=medium

  * New upstream snapshot setting the Bindfile_Option_Substitution attribute
    required by gprbuild (>= 2023~).

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 07 Aug 2022 12:04:51 +0000

gprconfig-kb (22.0.0-7) unstable; urgency=medium

  * Fix g++-BV compiler name in detect-gcc-11-compiler-as-well-as-gcc.diff.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 18 Jul 2022 07:38:01 +0000

gprconfig-kb (22.0.0-6) unstable; urgency=medium

  * Let gprbuild detect and use g{cc,++,fortran}-VERSION in addition to
    default versions.
  * Autopkgtest that Ada, C and mixed projects call the right compiler
    (here instead of in the tests for gprbuild).

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 15 Jul 2022 16:52:58 +0000

gprconfig-kb (22.0.0-5) unstable; urgency=medium

  * Ensure that gnat calls gcc with the right version suffix, without
    using the gnatgcc symbolic link. Closes: #1012509.
  * Stop checking *here* that Ada and C are built with the same version.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 14 Jul 2022 14:10:28 +0000

gprconfig-kb (22.0.0-4) unstable; urgency=medium

  * Compile C with gnatgcc. Check with autopkgtest.
    Close #992241 in dh-ada-library.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 28 May 2022 16:21:29 +0000

gprconfig-kb (22.0.0-3) unstable; urgency=medium

  * gprconfig.patch: patch library partial linker in the right line.
    Symptom: gprlib can’t call /usr/bin/gcc (or selects a wrong version).

 -- Nicolas Boulenguez <nicolas@debian.org>  Fri, 13 May 2022 09:45:54 +0000

gprconfig-kb (22.0.0-2) unstable; urgency=medium

  * Reupload to unstable for the gnat-11 transition
  * Configure branch names for git-buildpackage

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 08 May 2022 17:57:25 +0200

gprconfig-kb (22.0.0-1) experimental; urgency=medium

  * New upstream release.
    typo-ranlib.diff has been applied upstream.
  * Look for gprlib in /usr/libexec instead of deprecated /usr/lib.
    Upload to experimental because this breaks gprbuild < 2022.
  * Refresh patches, copyright, Standards-Version, metadata.
  * Remove unneeded filenamemangle uscan option.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 13 Mar 2022 12:33:33 +0000

gprconfig-kb (21.0.0-2) unstable; urgency=medium

  * Source-only upload allowing migration to testing.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 21 Nov 2020 12:03:55 +0000

gprconfig-kb (21.0.0-1) unstable; urgency=medium

  * Initial release closes: #972403.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sat, 17 Oct 2020 23:35:25 +0000
